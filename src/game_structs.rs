
#[derive(Clone, Copy, PartialEq, Eq, Hash, Debug)]
pub enum Player {
    Start,
    Second,
}

impl From<Player> for Cell {
    fn from(val: Player) -> Self {
        match val {
            Player::Start => Cell::Start,
            Player::Second => Cell::Second,
        }
    }
}

impl From<Player> for WinState {
    fn from(val: Player) -> Self {
        match val {
            Player::Start => WinState::Start,
            Player::Second => WinState::Second,
        }
    }
}

impl Player {
    pub fn next(&self) -> Player {
        match self {
            Player::Start => Player::Second,
            Player::Second => Player::Start,
        }
    }
}

#[derive(Copy, Clone, Debug, PartialEq, Eq, Hash)]
pub enum Cell {
    Start,
    Second,
    Empty,
}

#[allow(dead_code)]
pub const R: Cell = Cell::Start;
#[allow(dead_code)]
pub const B: Cell = Cell::Second;
#[allow(dead_code)]
pub const X: Cell = Cell::Empty;

#[derive(Copy, Clone, Ord, PartialOrd, PartialEq, Eq, Debug)]
pub enum WinState {
    Start,
    Even,
    Second,
}

type Field = [Cell; 9];

#[derive(PartialEq, Eq, Hash, Copy, Clone)]
pub struct GameState {
    _field: Field,
    _player: Player,
}

impl GameState {
    pub fn new() -> GameState {
        GameState {
            _field: [Cell::Empty; 9],
            _player: Player::Start,
        }
    }

    pub fn running_game(f: Field, p: Player) -> GameState {
        GameState {
            _field: f,
            _player: p,
        }
    }

    pub fn from_field(field: &[Cell; 9]) -> Option<GameState> {
        let (mut start, mut second) = (0_usize, 0_usize);
        for x in field {
            match x {
                Cell::Start => {
                    start += 1;
                }
                Cell::Second => {
                    second += 1;
                }
                Cell::Empty => {}
            };
        }
        if start == second {
            Some(GameState::running_game(*field, Player::Start))
        } else if start == second + 1 {
            Some(GameState::running_game(*field, Player::Second))
        } else {
            None
        }
    }

    pub fn print(&self) {
        println!(
            "{}\n===",
            match self._player {
                Player::Start => 'A',
                Player::Second => 'B',
            }
        );
        for line in [0_usize, 1, 2] {
            for col in [0_usize, 1, 2] {
                print!(
                    "{}",
                    match self._field[3 * line + col] {
                        Cell::Empty => '_',
                        Cell::Start => 'A',
                        Cell::Second => 'B',
                    }
                )
            }
            println!();
        }
    }

    pub fn generate_next_moves(&self) -> impl Iterator<Item = GameState> {
        let base_board = self._field;
        let player = self._player;
        base_board
            .into_iter()
            .enumerate()
            .filter_map(move |(idx, cell)| {
                if cell == Cell::Empty {
                    let mut new_board = base_board;
                    new_board[idx] = player.into();
                    Some(GameState::running_game(new_board, player.next()))
                } else {
                    None
                }
            })
    }

    pub fn won(&self, player: Player) -> bool {
        let player: Cell = player.into();
        // lines
        if [0_usize, 1, 2].into_iter().any(|line| {
            [0_usize, 1, 2]
                .into_iter()
                .all(|column| self._field[line * 3 + column] == player)
        }) {
            return true;
        }
        // columns
        if [0_usize, 1, 2].into_iter().any(|column| {
            [0_usize, 1, 2]
                .into_iter()
                .all(|line| self._field[line * 3 + column] == player)
        }) {
            return true;
        }
        // diagonals
        let arr: [[usize; 3]; 2] = [[0, 4, 8], [2, 4, 6]];
        if arr
            .into_iter()
            .any(|ref diagonal| diagonal.iter().all(|idx| self._field[*idx] == player))
        {
            return true;
        }
        false
    }

    pub fn player(&self) -> Player {
        self._player
    }

    pub fn field(&self) -> &Field {
        &self._field
    }
}
