mod game_structs;

use game_structs::*;
use std::collections::{HashMap, HashSet, VecDeque};

fn main() {
    let (graph, mut winstates) = GameGraph::expand_graph();
    graph.minmax(&mut winstates);
    let field = [
        X, X, X, //
        X, X, X, //
        X, X, X,
    ];
    let start_idx = *graph
        .indexes
        .get(&GameState::from_field(&field).unwrap())
        .unwrap();
    let mut player_cfg: [PruneTake; 2] = [PruneTake::AllSensible, PruneTake::AllSensible];
    let statistical_min_max = graph.statistical_minmax_for_player(&winstates, Player::Start);
    player_cfg[Player::Start as usize] = PruneTake::Statistically(statistical_min_max.clone());
    player_cfg[Player::Second as usize] = PruneTake::AllPossible;
    let pruned_nodes = graph.traverse_pruning(&winstates, start_idx, &player_cfg);
    graph.print_graph(&winstates, &pruned_nodes, |idx| {
        statistical_min_max
            .get(&idx)
            .map(|val| format!("chance for R: {}", val.0))
    });
}

type NodeIdx = usize;

struct Node {
    next: Vec<usize>,
    prev: Vec<usize>,
    data: GameState,
}

struct GameGraph {
    nodes: HashMap<NodeIdx, Node>,
    indexes: HashMap<GameState, NodeIdx>,
}

#[derive(PartialEq, Clone)]
enum PruneTake {
    Any,
    AllPossible,
    AllSensible,
    Statistically(HashMap<NodeIdx, (f64, Option<NodeIdx>)>),
}

impl GameGraph {
    fn new() -> GameGraph {
        GameGraph {
            nodes: HashMap::new(),
            indexes: HashMap::new(),
        }
    }

    // assumes
    fn insert(&mut self, data: GameState) -> (NodeIdx, &mut Node, bool /* newly-created? */) {
        let next_idx = self.nodes.len();
        match self.indexes.entry(data) {
            std::collections::hash_map::Entry::Occupied(occ) => {
                match self.nodes.get_mut(occ.get()) {
                    Some(v) => {
                        return (*occ.get(), v, false);
                    }
                    None => {
                        panic!("Index exists, but game state not, bug.");
                    }
                }
            }
            std::collections::hash_map::Entry::Vacant(v) => {
                v.insert(next_idx);
            }
        }
        let value = match self.nodes.entry(next_idx) {
            std::collections::hash_map::Entry::Occupied(_) => {
                panic!("Index already exists, bug.")
            }
            std::collections::hash_map::Entry::Vacant(v) => v.insert(Node {
                next: Vec::new(),
                prev: Vec::new(),
                data,
            }),
        };
        (next_idx, value, true)
    }

    // returns successor nodes that were created anew
    fn expand_node(
        &mut self,
        idx: NodeIdx,
    ) -> (Vec<(NodeIdx, GameState)>, usize /* num_successors */) {
        let node = self.nodes.get_mut(&idx).unwrap_or_else(|| {
            panic!("[GameGraph::expand_node] Index does not exist in graph.");
        });
        let mut res = Vec::<(NodeIdx, GameState)>::with_capacity(9);
        let mut successor_nodes = Vec::<NodeIdx>::with_capacity(9);
        for field in node.data.generate_next_moves() {
            let (next_idx, next_node, newly_inserted) = self.insert(field);
            next_node.prev.push(idx);
            successor_nodes.push(next_idx);
            if newly_inserted {
                res.push((next_idx, field));
            }
        }
        let node = self.nodes.get_mut(&idx).unwrap_or_else(|| {
            panic!("[GameGraph::expand_node] Index does not exist in graph.");
        });
        let num_successors = successor_nodes.len();
        node.next.append(&mut successor_nodes);
        (res, num_successors)
    }

    fn expand_graph() -> (GameGraph, HashMap<NodeIdx, WinState>) {
        let mut graph = GameGraph::new();
        let mut winstates = HashMap::<NodeIdx, WinState>::new();
        let mut unexpanded_states = VecDeque::<NodeIdx>::new();
        {
            let initial_board = GameState::new();
            unexpanded_states.push_back(graph.insert(initial_board).0);
        }

        while let Some(current) = unexpanded_states.pop_front() {
            let (expanded, num_successors) = graph.expand_node(current);
            if num_successors == 0 {
                winstates.insert(current, WinState::Even);
            }
            for (idx, field) in expanded {
                let might_have_won = field.player().next(); // ~ previous()
                if field.won(might_have_won) {
                    winstates.insert(idx, might_have_won.into());
                } else {
                    unexpanded_states.push_back(idx);
                }
            }
        }
        (graph, winstates)
    }

    fn minmax(&self, winstates: &mut HashMap<NodeIdx, WinState>) {
        let mut num_successors = HashMap::<NodeIdx, usize>::new();
        let mut search_front =
            VecDeque::<(NodeIdx, Vec<NodeIdx> /* predecessors */, WinState)>::new();
        for (idx, node) in self.nodes.iter() {
            if !node.next.is_empty() {
                num_successors.insert(*idx, node.next.len());
            } else {
                search_front.push_back((
                    *idx,
                    self.nodes.get(idx).unwrap().prev.clone(),
                    *winstates.get(idx).unwrap(),
                ));
            }
            // check invariant
            if node.next.is_empty() != winstates.contains_key(idx) {
                panic!("Something is wrong");
            }
        }
        while let Some((idx, pred, winstate)) = search_front.pop_front() {
            // println!(
            //     "Taking {} from search front, has {} predecessors",
            //     idx,
            //     pred.len()
            // );
            for pred_idx in pred {
                // println!("\tLooking at predecessor {}", pred_idx);
                // 1. Update WinState in `winstates`
                let pred_node = self.nodes.get(&pred_idx).unwrap();
                let pred_winstate = match winstates.entry(pred_idx) {
                    std::collections::hash_map::Entry::Vacant(v) => *v.insert(winstate),
                    std::collections::hash_map::Entry::Occupied(mut o) => {
                        let entry = o.get_mut();
                        match pred_node.data.player() {
                            Player::Start => {
                                *entry = std::cmp::min(*entry, winstate);
                            }
                            Player::Second => {
                                *entry = std::cmp::max(*entry, winstate);
                            }
                        }
                        *entry
                    }
                };

                // 2. Update `num_successors`: If zero, move to `search_front`
                match num_successors.entry(pred_idx) {
                    std::collections::hash_map::Entry::Vacant(_) => {
                        println!(
                            "Predecessor is in search front: {}",
                            search_front.iter().any(|(i, _, _)| *i == pred_idx)
                        );
                        self.nodes.get(&pred_idx).unwrap().data.print();
                        println!("\n==>\n");
                        self.nodes.get(&idx).unwrap().data.print();
                        panic!(
                            "[GameGraph::minmax()] num_successors not found \
                             for node."
                        )
                    }
                    std::collections::hash_map::Entry::Occupied(mut o) => {
                        let entry = o.get_mut();
                        *entry -= 1;
                        if *entry == 0 {
                            o.remove_entry();
                            search_front.push_back((
                                pred_idx,
                                pred_node.prev.clone(),
                                pred_winstate,
                            ));
                        }
                    }
                }
            }
        }
    }

    /*
     * Player plays optimally, opponent plays randomly
     */
    fn statistical_minmax_for_player(
        &self,
        winstates: &HashMap<NodeIdx, WinState>,
        player: Player,
    ) -> HashMap<NodeIdx, (f64, Option<NodeIdx>)> {
        let mut res = HashMap::<
            NodeIdx,
            (
                f64,             /* probability */
                Option<NodeIdx>, /* when picking this next state */
            ),
        >::new();
        let mut num_successors = HashMap::<NodeIdx, usize>::new();
        let mut search_front = VecDeque::<NodeIdx>::new();
        let desired_winstate: WinState = player.into();
        for (idx, node) in self.nodes.iter() {
            if !node.next.is_empty() {
                num_successors.insert(*idx, node.next.len());
            } else {
                search_front.push_back(*idx);
            }
        }
        while let Some(idx) = search_front.pop_front() {
            let node = self.nodes.get(&idx).unwrap();
            // 1. Set own win chance
            if node.next.is_empty() {
                // 1a) A player has won
                if *winstates.get(&idx).unwrap() == desired_winstate {
                    res.insert(idx, (1.0, None));
                } else {
                    res.insert(idx, (0.0, None));
                }
            } else {
                // 1b) Game is ongoing
                let my_winstate = *winstates.get(&idx).unwrap();
                let child_win_chances = node
                    .next
                    .iter()
                    .map(|succ_idx| (*succ_idx, res.get(succ_idx).unwrap().0));
                if node.data.player() == player {
                    // 1b-a) My turn, I can select the best
                    // Filter iterator to consider only those children that don't
                    // degrade my winstate
                    let mut child_win_chances = child_win_chances.filter(|(succ_idx, _)| {
                        let child_win_state = *winstates.get(succ_idx).unwrap();
                        my_winstate == child_win_state
                    });
                    let (picked_successor, max_value) = child_win_chances.next().unwrap();
                    let (picked_successor, max_value) = child_win_chances.fold(
                        (picked_successor, max_value),
                        |(acc_successor, acc_value), (alt_successor, alt_value)| {
                            if alt_value.partial_cmp(&acc_value).unwrap()
                                == core::cmp::Ordering::Greater
                            {
                                (alt_successor, alt_value)
                            } else {
                                (acc_successor, acc_value)
                            }
                        },
                    );
                    // println!("Setting win chance for {} to {} with successor {}", idx, max_value, picked_successor);
                    res.insert(idx, (max_value, Some(picked_successor)));
                } else {
                    // 1b-b) Opponent's turn, he does whatever
                    let sum: f64 = child_win_chances.map(|(_, chance)| chance).sum();
                    let average = sum / node.next.len() as f64;
                    res.insert(idx, (average, None));
                }
            }
            // 2. Update `num_successors`: If zero, move to `search_front`
            for pred_idx in node.prev.iter().copied() {
                match num_successors.entry(pred_idx) {
                    std::collections::hash_map::Entry::Vacant(_) => {
                        panic!(
                            "[GameGraph::statistical_minmax()] num_successors \
                             not found for node."
                        )
                    }
                    std::collections::hash_map::Entry::Occupied(mut o) => {
                        let entry = o.get_mut();
                        *entry -= 1;
                        if *entry == 0 {
                            let _ = entry;
                            o.remove_entry();
                            search_front.push_back(pred_idx);
                        }
                    }
                }
            }
        }

        res
    }

    fn traverse_pruning(
        &self,
        winstates: &HashMap<NodeIdx, WinState>,
        start_idx: NodeIdx,
        player_cfg: &[PruneTake; 2],
    ) -> HashSet<NodeIdx> {
        let mut result = HashSet::<NodeIdx>::new();
        let mut search_front = VecDeque::<NodeIdx>::new();
        result.insert(start_idx);
        search_front.push_back(start_idx);
        while let Some(idx) = search_front.pop_front() {
            let node = self.nodes.get(&idx).unwrap();
            let player_cfg = &player_cfg[node.data.player() as usize];
            if let PruneTake::Statistically(ref map) = player_cfg {
                if node.next.is_empty() {
                    continue;
                }
                let next_idx = map.get(&idx).unwrap().1.unwrap();
                if result.contains(&next_idx) {
                    continue;
                }
                result.insert(next_idx);
                search_front.push_back(next_idx);
                continue;
            }
            for next_idx in node.next.iter().copied() {
                if result.contains(&next_idx) {
                    continue;
                }
                let winstate = *winstates.get(&idx).unwrap();
                let next_winstate = *winstates.get(&next_idx).unwrap();
                if winstate == next_winstate || player_cfg == &PruneTake::AllPossible {
                    result.insert(next_idx);
                    search_front.push_back(next_idx);
                    if player_cfg == &PruneTake::Any {
                        break;
                    }
                }
            }
        }
        result
    }

    fn print_graph<F: Fn(NodeIdx) -> Option<String>>(
        &self,
        winstates: &HashMap<NodeIdx, WinState>,
        nodes: &HashSet<NodeIdx>,
        additional_node_info: F,
    ) {
        println!("digraph G {{\nnode[shape=\"rectangle\"];");
        for idx in nodes.iter().cloned() {
            let node = self.nodes.get(&idx).unwrap();
            let won = node.data.won(node.data.player().next());
            let whose_turn = match node.data.player() {
                Player::Start => "red",
                Player::Second => "blue",
            };
            let (color, wincolor) = match winstates.get(&idx).unwrap() {
                WinState::Start => ("red", "orangered"),
                WinState::Even => ("black", "black"),
                WinState::Second => ("blue", "dodgerblue"),
            };
            print!("node_{} [fontname=monospace, ", idx);
            if won {
                print!("style=bold, fillcolor=\"{}\", style=filled, ", wincolor);
            } else {
                print!("color=\"{}\", fontcolor=\"{}\", ", color, color);
            }
            print!("label=\"");
            if !won {
                print!("{}'s turn\\n", whose_turn);
            }
            if let Some(s) = additional_node_info(idx) {
                print!("{}\\n", s);
            }
            for line in [0_usize, 1, 2] {
                for col in [0_usize, 1, 2] {
                    print!(
                        "{}",
                        match node.data.field()[3 * line + col] {
                            Cell::Empty => '_',
                            Cell::Start => 'R',
                            Cell::Second => 'B',
                        }
                    )
                }
                print!("\\n");
            }
            println!("\"];");
        }
        for idx in nodes {
            let node = self.nodes.get(idx).unwrap();
            for next_idx in &node.next {
                let color = match winstates.get(next_idx).unwrap() {
                    WinState::Start => "red",
                    WinState::Even => "black",
                    WinState::Second => "blue",
                };
                if nodes.contains(next_idx) {
                    println!("node_{} -> node_{} [color=\"{}\"];", idx, next_idx, color);
                }
            }
        }
        println!("}}");
    }
}
